package languageExpression;

import stateVisitor.TurtleVisitable;
import turtle.Turtle;

public interface ICommandExpression extends TurtleVisitable {
    void interpret(Turtle turtle);
    String toString();
}
