package languageExpression;

public class IntegerExpression {

    private final int value;

    public IntegerExpression(int value){
        this.value = value;
    }

    public int getValue(){
        return value;
    }

    public String toString() {
        return "Integer Value " + value;
    }
}
