package languageExpression;

import stateVisitor.TurtleVisitor;
import turtle.Turtle;

public class MoveExpression implements ICommandExpression {

    // right operand for move operation
    private IntegerExpression number;
    private Turtle turtle;

    public Turtle getTurtleState(){
        return turtle;
    }

    public void interpret(Turtle turtle) {
        turtle.move(number.getValue());
    }

    public MoveExpression(IntegerExpression number){
        this.number = number;
    }

    @Override
    public void accept(TurtleVisitor visitor) {
        visitor.visit(this);
    }

    public String toString() {
        return "Move " + number.getValue();
    }
}
