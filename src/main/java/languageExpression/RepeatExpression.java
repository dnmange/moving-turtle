package languageExpression;

import stateVisitor.TurtleVisitor;
import turtle.Turtle;

import java.util.List;

public class RepeatExpression implements ICommandExpression {

    private List<ICommandExpression> expressions;
    private int repitionCount;

    public RepeatExpression(IntegerExpression repitionCountExpression, List<ICommandExpression> expressions){

        this.setRepitionCount(repitionCountExpression.getValue());
        this.setExpressions(expressions);
    }

    @Override
    public void interpret(Turtle turtle) {

        int count = getRepitionCount();
        while (count-- != 0) {
            for (ICommandExpression expression : getExpressions()){
                expression.interpret(turtle);
            }
        }
    }

    @Override
    public String toString() {
        return "Repetition count: "+ getRepitionCount() + " " + "Expressions: {\n" + expressions.toString() + "\n}";
    }

    @Override
    public void accept(TurtleVisitor visitor) {
        visitor.visit(this);
    }

    public List<ICommandExpression> getExpressions() {
        return expressions;
    }

    public void setExpressions(List<ICommandExpression> expressions) {
        this.expressions = expressions;
    }

    public int getRepitionCount() {
        return repitionCount;
    }

    public void setRepitionCount(int repitionCount) {
        this.repitionCount = repitionCount;
    }
}
