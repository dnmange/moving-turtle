package languageExpression;

import stateVisitor.TurtleVisitor;
import turtle.Turtle;

public class TurnExpression implements ICommandExpression {

    private IntegerExpression number;

    private Turtle turtle;

    public Turtle getTurtleState(){
        return turtle;
    }

    public void interpret(Turtle turtle) {
        turtle.turn(number.getValue());
    }

    public TurnExpression(IntegerExpression number){
        this.number = number;
    }

    @Override
    public void accept(TurtleVisitor visitor) {
        visitor.visit(this);
    }

    public String toString() {
        return "Turn " + number.getValue() + " degrees";
    }
}
