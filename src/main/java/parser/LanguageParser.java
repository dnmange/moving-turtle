package parser;

import languageExpression.*;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class LanguageParser {

    private HashMap<String, Integer> variablesMap;

    /*
    * Returns a root node of the tree of type ICommandExpression which can be traversed
    * */
    public ICommandExpression constructSyntaxTree(String fileName) throws IOException, NumberFormatException {

        File file = new File(fileName);
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));

        variablesMap = new HashMap<>();
        return constructRepeatExpression(1, new ArrayList<ICommandExpression>(), bufferedReader);
    }

    /*
    * This is recursive function for creating repeatExpression inside repeatExpression
    * */
    private ICommandExpression constructRepeatExpression(int repeatCount, List<ICommandExpression> commandExpressions,
                                  BufferedReader bufferedReader)
            throws IOException, NumberFormatException{

        String line;
        List<ICommandExpression> expressions = new ArrayList<>();

        while ((line = bufferedReader.readLine()) != null) {

            // remove whitespaces to support indentation
            line = line.trim();

            if (line.equals("end")){
                return new RepeatExpression(new IntegerExpression(repeatCount), expressions);
            }

            String[] numberSplit = line.split(" ");
            IntegerExpression rightOperand = constructIntExp(numberSplit[1]);

            if (numberSplit[0].charAt(0) == '#'){
                variablesMap.put(numberSplit[0].substring(1), rightOperand.getValue());
            }
            else if (numberSplit[0].equals("move"))
                expressions.add(new MoveExpression(rightOperand));
            else if (numberSplit[0].equals("turn"))
                expressions.add(new TurnExpression(rightOperand));
            else if (numberSplit[0].equals("repeat")){

                int value = rightOperand.getValue();
                
                RepeatExpression repeatExpression = 
                        (RepeatExpression) constructRepeatExpression(value, new ArrayList<ICommandExpression>(), bufferedReader);
                expressions.add(repeatExpression);
            }
        }

        return new RepeatExpression(new IntegerExpression(repeatCount), expressions);
    }

    /*
    * constructs integer expression from variables map or actual integer
    * */
    private IntegerExpression constructIntExp(String number) throws NumberFormatException{

        int value;

        if(variablesMap.containsKey(number))
            value = variablesMap.get(number);
        else
            value = Integer.parseInt(number);

        return new IntegerExpression(value);
    }

}
