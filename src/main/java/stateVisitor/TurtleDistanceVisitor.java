package stateVisitor;

import languageExpression.ICommandExpression;
import languageExpression.MoveExpression;
import languageExpression.RepeatExpression;
import languageExpression.TurnExpression;
import turtle.Point;
import turtle.Turtle;

import java.util.List;

public class TurtleDistanceVisitor implements TurtleVisitor {

    private Turtle turtle;
    private double totalDistance;

    public TurtleDistanceVisitor(Turtle turtle){
        this.turtle = turtle;
        setTotalDistance(0);
    }

    @Override
    public void visit(MoveExpression expression) {

        // making a copy instead of using reference
        Point oldPoint = new Point(turtle.location());

        expression.interpret(turtle);
        setTotalDistance(getTotalDistance() + oldPoint.distanceFromAnotherPoint(turtle.location()));
    }

    @Override
    public void visit(TurnExpression expression) {
        expression.interpret(turtle);
    }

    @Override
    public void visit(RepeatExpression repeatExpression) {

        int count = repeatExpression.getRepitionCount();
        List<ICommandExpression> expressions = repeatExpression.getExpressions();
        while (count-- != 0) {
            for (ICommandExpression expression : expressions){

                // calling accept instead interpret allow us to pass visitor to the child nodes
                expression.accept(this);
            }
        }
    }

    public double getTotalDistance() {
        return totalDistance;
    }

    public void setTotalDistance(double totalDistance) {
        this.totalDistance = totalDistance;
    }
}
