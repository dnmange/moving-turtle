package stateVisitor;

import languageExpression.ICommandExpression;
import languageExpression.MoveExpression;
import languageExpression.RepeatExpression;
import languageExpression.TurnExpression;
import turtle.Point;
import turtle.Turtle;
import turtle.memento.TurtleCareTaker;
import turtle.memento.TurtleMemento;

import java.util.Iterator;
import java.util.List;

public class TurtleStepVisitor implements TurtleVisitor, Iterable {

    private TurtleCareTaker turtleCareTaker;
    private Turtle turtle;

    public TurtleStepVisitor(Turtle turtle){

        this.turtle = turtle;
        turtleCareTaker = new TurtleCareTaker();
    }

    @Override
    public void visit(MoveExpression expression) {

        expression.interpret(turtle);
        turtleCareTaker.addStateToList(createTurtleMemento());
    }

    @Override
    public void visit(TurnExpression expression) {

        expression.interpret(turtle);
        turtleCareTaker.addStateToList(createTurtleMemento());
    }

    @Override
    public void visit(RepeatExpression repeatExpression) {

        int count = repeatExpression.getRepitionCount();
        List<ICommandExpression> expressions = repeatExpression.getExpressions();
        while (count-- != 0) {
            for (ICommandExpression expression : expressions){

                // calling accept instead interpret allow us to pass visitor to the child nodes
                expression.accept(this);
            }
        }
    }

    private TurtleMemento createTurtleMemento(){
        return new TurtleMemento(turtle.direction(), new Point(turtle.location()));
    }

    public Turtle restoreFromMemento(TurtleMemento memento){
        return new Turtle(memento.getDirection(), memento.getLocation());
    }

    @Override
    public Iterator iterator() {
        return turtleCareTaker.getTurtleMementoList().iterator();
    }

    public TurtleCareTaker getTurtleCareTaker(){
        return turtleCareTaker;
    }
}
