package stateVisitor;

public interface TurtleVisitable {

    void accept(TurtleVisitor visitor);
}
