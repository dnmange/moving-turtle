package stateVisitor;

import languageExpression.MoveExpression;
import languageExpression.RepeatExpression;
import languageExpression.TurnExpression;

public interface TurtleVisitor {

    void visit(MoveExpression visitable);
    void visit(TurnExpression visitable);
    void visit(RepeatExpression visitable);
}
