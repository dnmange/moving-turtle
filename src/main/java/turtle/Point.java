package turtle;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class Point {

    private double x;
    private double y;

    public Point(double x, double y){
        this.setX(x);
        this.setY(y);
    }

    public Point(Point point){
        this.setX(point.getX());
        this.setY(point.getY());
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

     /*
     * hashcode is overridden so as to maintain consistency with equals method
     * */
    @Override
    public int hashCode(){
        return (int)((x+y)% Integer.MAX_VALUE);
    }

    @Override
    public boolean equals(Object obj){
        if (obj == this) return true;

        if (obj == null) return false;

        if (!(obj instanceof Point))
            return false;

        Point point = (Point) obj;

        // 0.1 is threshold of equality as co-ordinates can have recurring float values
        return Math.abs(this.x - point.getX()) <= 0.1 && Math.abs(this.y - point.getY()) <= 0.1;
    }

    public double distanceFromAnotherPoint(Point point){

        double x = this.getX() - point.getX();
        double y = this.getY() - point.getY();
        return Math.sqrt(x * x + y * y);
    }

    public String toString() {

        NumberFormat decimalFormat = new DecimalFormat("#0.00");
        String x = "X: " + decimalFormat.format(this.getX());
        String y = "Y: " + decimalFormat.format(this.getY());

        return "(" + x + " " + y + ")";
    }
}
