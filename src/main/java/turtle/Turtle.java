package turtle;

public class Turtle implements Cloneable{

    private Point point;
    private int direction;
    private static final double PI = 3.14159;
    private boolean draw;

    public Object clone() throws CloneNotSupportedException{

        return super.clone();
    }

    public Turtle(){

        point = new Point(0,0);
        direction = 0;
        draw = false;
    }

    public Turtle(int direction, Point coordinates){
        this.point = coordinates;
        this.direction = direction;
    }

    /*
    * x and y co-ordinate of the turtle is shifted acc to distance
    * and degrees
    * */
    public void move(int distance){

        double radians  = direction() * PI / 180;

        point.setX(point.getX() + Math.cos(radians) * distance);
        point.setY(point.getY() + Math.sin(radians) * distance);
    }

    /*
    * changes the directions of the turtle acc to the degrees
    * */
    public void turn(int degrees){

        degrees = degrees % 360;

        if (degrees < 0){
            degrees = 360 + degrees;
        }

        direction = (direction + degrees)%360;
    }

    /*
    * while moving line is not drawn
    * */
    public void penUp(){

        this.draw = false;
    }

    /*
    * while moving line is drawn
    * */
    public void penDown(){

        this.draw = true;
    }

    /*
    * returns the orientation of the turtle
    * */
    public int direction(){
        return direction;
    }


    public Point location(){
        return point;
    }

    public String toString() {

        return "Location: " + point + "Draw: " + this.draw + "Direction: " + this.direction + " degrees";
    }
}
