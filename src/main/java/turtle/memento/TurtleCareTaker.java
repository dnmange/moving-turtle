package turtle.memento;

import java.util.ArrayList;
import java.util.List;

public class TurtleCareTaker {
    private List<TurtleMemento> turtleMementoList;

    public TurtleCareTaker(){
        turtleMementoList = new ArrayList<>();
    }

    public void addStateToList(TurtleMemento memento){
        getTurtleMementoList().add(memento);
    }

    public TurtleMemento getMementoFromList(int index){

        if (index < 0 || index > getTurtleMementoList().size()-1)
            throw new IndexOutOfBoundsException();

        return getTurtleMementoList().get(index);
    }

    public int size(){
        return getTurtleMementoList().size();
    }

    public List<TurtleMemento> getTurtleMementoList() {
        return turtleMementoList;
    }

    public String toString() {

        return turtleMementoList.toString();
    }

}
