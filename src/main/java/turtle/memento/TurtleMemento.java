package turtle.memento;

import turtle.Point;

public class TurtleMemento {

    private int direction;
    private Point location;

    public TurtleMemento(int direction, Point location){
        this.direction = direction;
        this.location = location;
    }

    public int getDirection(){
        return direction;
    }

    public Point getLocation(){
        return location;
    }

    public String toString(){
        return "Direction: "+direction+"Location: "+location;
    }
}
