import languageExpression.ICommandExpression;
import org.junit.jupiter.api.Test;
import parser.LanguageParser;
import stateVisitor.TurtleDistanceVisitor;
import stateVisitor.TurtleStepVisitor;
import turtle.Point;
import turtle.Turtle;
import turtle.memento.TurtleCareTaker;
import turtle.memento.TurtleMemento;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TurtleTest {

    @Test
    public void testInterpreterBasicFLow() throws IOException {

        String fileName = System.getProperty("user.dir")+"/sample-inputs/basic-input.txt";
        testInterpreter(fileName, new Point(22.99, 27.5));
    }

    @Test
    public void testInterpreterWithRepeatFlow() throws IOException {

        String fileName = System.getProperty("user.dir")+"/sample-inputs/repeat-input.txt";
        testInterpreter(fileName, new Point(10, 0));
    }

    private void testInterpreter(String fileName, Point expectedPoint) throws IOException {

        Turtle turtle = new Turtle();

        LanguageParser languageParser = new LanguageParser();

        ICommandExpression expression =
                languageParser.constructSyntaxTree(fileName);

        expression.interpret(turtle);
        assertTrue(expectedPoint.equals(turtle.location()));
    }

    @Test
    public void testStepVisitorBasicFlow() throws IOException {

        String fileName = System.getProperty("user.dir")+"/sample-inputs/basic-input.txt";
        List<Point> expectedCoordinates = new ArrayList<>();

        expectedCoordinates.add(new Point(10,0));
        expectedCoordinates.add(new Point(10,0));
        expectedCoordinates.add(new Point(10,20));
        expectedCoordinates.add(new Point(10,20));
        expectedCoordinates.add(new Point(22.9,27.5));

        testStepVisitor(fileName, expectedCoordinates);
    }

    @Test
    public void testStepVisitorRepeatFlow() throws IOException {

        String fileName = System.getProperty("user.dir")+"/sample-inputs/repeat-input.txt";
        List<Point> expectedCoordinates = new ArrayList<>();

        expectedCoordinates.add(new Point(10,0));
        expectedCoordinates.add(new Point(20,0));
        expectedCoordinates.add(new Point(20,0));
        expectedCoordinates.add(new Point(20,10));
        expectedCoordinates.add(new Point(20,10));
        expectedCoordinates.add(new Point(10,10));
        expectedCoordinates.add(new Point(10,10));
        expectedCoordinates.add(new Point(10,0));
        expectedCoordinates.add(new Point(10,0));

        testStepVisitor(fileName, expectedCoordinates);
    }

    private void testStepVisitor(String fileName, List<Point> expectedCoordinates) throws IOException {

        Turtle turtle = new Turtle();
        LanguageParser languageParser = new LanguageParser();

        ICommandExpression expression =
                languageParser.constructSyntaxTree(fileName);

        TurtleStepVisitor visitor = new TurtleStepVisitor(turtle);
        expression.accept(visitor);

        TurtleCareTaker turtleCareTaker = visitor.getTurtleCareTaker();
        List<TurtleMemento> states = turtleCareTaker.getTurtleMementoList();

        int i = 0;
        for (TurtleMemento turtleMemento : states){
            assertTrue(expectedCoordinates.get(i).equals(turtleMemento.getLocation()));
            i++;
        }
    }

    @Test
    public void testDistanceBasicFLow() throws IOException {

        String fileName = System.getProperty("user.dir")+"/sample-inputs/basic-input.txt";
        testDistanceVisitor(fileName, 45);
    }

    @Test
    public void testDistanceWithRepeatFlow() throws IOException {

        String fileName = System.getProperty("user.dir")+"/sample-inputs/repeat-input.txt";
        testDistanceVisitor(fileName, 50);
    }

    private void testDistanceVisitor(String fileName, double expectedDistance) throws IOException {

        Turtle turtle = new Turtle();

        LanguageParser languageParser = new LanguageParser();

        ICommandExpression expression =
                languageParser.constructSyntaxTree(fileName);

        TurtleDistanceVisitor visitor = new TurtleDistanceVisitor(turtle);
        expression.accept(visitor);

        assertEquals(expectedDistance, visitor.getTotalDistance());
    }

    @Test
    public void testParserNumberFormatException() {

        String fileName = System.getProperty("user.dir")+"/sample-inputs/illegal-input.txt";
        Turtle turtle = new Turtle();

        LanguageParser languageParser = new LanguageParser();

        assertThrows(NumberFormatException.class, () -> {
                languageParser.constructSyntaxTree(fileName);
        });
    }
}
